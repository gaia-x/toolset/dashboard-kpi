import { projects } from "@/projects";
import { Gitlab } from "@gitbeaker/node";
import fs from "fs";

export const api = new Gitlab({
  host: process.env.GITLAB_HOST,
  token: process.env.GITLAB_TOKEN,
});

export const getKpisAndSave = async () => {
  const projectsWithKpis = await Promise.all(
    projects.map(async (project) => {
      try {
        const mergeRequests = keepRelevantData(
          await api.MergeRequests.all({
            projectId: project.projectId,
          })
        );

        const mergeRequestsDateLabels = getDatesLabels(mergeRequests);

        const mergeRequestsKpis = countKpis(
          mergeRequestsDateLabels,
          mergeRequests
        );

        const issues = keepRelevantData(
          await api.Issues.all({ projectId: project.projectId })
        );

        const issuesDateLabels = getDatesLabels(issues);

        const issuesKpis = countKpis(issuesDateLabels, issues);

        return {
          name: project.name,
          mergeRequests: mergeRequestsKpis,
          issues: issuesKpis,
        };
      } catch (error) {
        console.log(error);
      }
    })
  );

  try {
    const fileExists = fs.existsSync("./kpis.json");
    if (fileExists) {
      fs.unlinkSync("./kpis.json");
    }

    fs.writeFileSync(
      "./kpis.json",
      JSON.stringify(
        {
          data: {
            date: new Date().toJSON().split("T")[0],
            kpis: projectsWithKpis,
          },
        },
        null,
        2
      )
    );

    console.log("KPIs saved successfully!");
  } catch (error) {
    console.log("Fail to save KPI: " + error);
  }

  return projectsWithKpis;
};

function keepRelevantData(data) {
  return data.map((mr) => ({
    created_at: mr.created_at,
    closed_at: mr.closed_at,
    merged_at: mr.merged_at,
  }));
}

function getDatesLabels(jsonArray) {
  const startDate = new Date(
    jsonArray.reduce((prev, current) => {
      const date = Date.parse(current.created_at);
      return date < prev ? date : prev;
    }, Infinity)
  );

  const mergedOrClosedAt = jsonArray
    .map((obj) => obj.merged_at || obj.closed_at)
    .find((date) => date != null);
  const endDate = new Date(Date.parse(mergedOrClosedAt));

  const startYear = startDate.getFullYear();
  const startMonth = startDate.getMonth();
  const endYear = endDate.getFullYear();
  const endMonth = endDate.getMonth();

  const months = [];

  for (let year = startYear; year <= endYear; year++) {
    const start = year === startYear ? startMonth : 0;
    const end = year === endYear ? endMonth : 11;

    for (let month = start; month <= end; month++) {
      const date = new Date(year, month, 1);
      const formattedMonth = date.toLocaleDateString("en-US", {
        month: "2-digit",
        year: "numeric",
      });
      months.push(formattedMonth);
    }
  }

  return months;
}

function countKpis(months, data) {
  const isDateMatching = (date, month) => {
    return (
      date.getFullYear() === parseInt(month.split("/")[1]) &&
      date.getMonth() === parseInt(month.split("/")[0]) - 1
    );
  };

  const getMatchingDateCount = (datePropName, month) => {
    const matchingData = data.filter((elm) =>
      isDateMatching(new Date(elm[datePropName]), month)
    );

    return matchingData.length;
  };

  const kpis = months.map((month) => {
    const created = getMatchingDateCount("created_at", month);
    const merged = getMatchingDateCount("merged_at", month);
    const closed = getMatchingDateCount("closed_at", month);

    return {
      [month]: {
        created,
        merged,
        closed,
        open: 0,
      },
    };
  });

  // Add open KPI
  kpis.forEach((kpi, index) => {
    if (index === 0) return;

    const previousKpi = kpis[index - 1];
    const currentKpiKey = Object.keys(kpi)[0];
    const previousKpiKey = Object.keys(previousKpi)[0];

    const currentKpi = kpi[currentKpiKey];
    const previousKpiOpen = previousKpi[previousKpiKey].open;

    const open =
      previousKpiOpen +
      currentKpi.created -
      (currentKpi.merged || 0) -
      currentKpi.closed;

    currentKpi.open = open < 0 ? 0 : open;
  });

  return arrayIntoObject(kpis);
}

function arrayIntoObject(array) {
  return array.reduce((acc, obj) => {
    const key = Object.keys(obj)[0];
    acc[key] = obj[key];
    return acc;
  }, {});
}
