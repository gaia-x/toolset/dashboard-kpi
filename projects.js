export const projects = [
  {
    name: "Trust Framework",
    projectId: 32704306,
  },
  {
    name: "Architecture Document",
    projectId: 16504188,
  },
  {
    name: "Identity and Access Management",
    projectId: 34010846,
  },
  {
    name: "Policy Rules and Label",
    projectId: 27890663,
  },
  {
    name: "Data Exchange",
    projectId: 35106943,
  },
  {
    name: "Compliance (Software)",
    projectId: 35272018,
  },
  {
    name: "Registry (Software)",
    projectId: 33995987,
  },
  {
    name: "Notary (Software)",
    projectId: 43170458,
  },
];
