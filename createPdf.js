const puppeteer = require("puppeteer");
const fs = require("fs");
const sharp = require("sharp");
const { PDFDocument } = require("pdf-lib");

const createPdf = async () => {
  await takeScreenshot();
  await convertPngToPdf();
};

createPdf().then((r) => {
  console.log("PDF created successfully!");
  fs.unlinkSync("./screenshot.png");
});

async function takeScreenshot() {
  const browser = await puppeteer.launch({ headless: "new" });

  const page = await browser.newPage();

  await page.goto("http://localhost:3000");
  await page.setViewport({ width: 1920, height: 1080 });

  function delay(time) {
    return new Promise(function (resolve) {
      setTimeout(resolve, time);
    });
  }

  await delay(1000);

  await page.screenshot({ path: "screenshot.png", fullPage: true });

  await browser.close();
}

// Function to convert PNG to PDF
async function convertPngToPdf() {
  // Usage
  const pngPath = "screenshot.png";
  const outputPath = "kpis.pdf";

  // Read the PNG image
  const pngBuffer = fs.readFileSync(pngPath);

  // Use sharp to get the PNG image metadata
  const { width, height } = await sharp(pngBuffer).metadata();

  // Create a new PDF document
  const pdfDoc = await PDFDocument.create();
  const pdfPage = pdfDoc.addPage([width, height]);

  // Convert the PNG buffer to an ArrayBuffer for pdf-lib
  const pngArrayBuffer = new Uint8Array(pngBuffer).buffer;

  // Embed the PNG image in the PDF document
  const pngImage = await pdfDoc.embedPng(pngArrayBuffer);
  pdfPage.drawImage(pngImage, { x: 0, y: 0, width, height });

  // Save the PDF document to a buffer
  const pdfBytes = await pdfDoc.save();

  // Write the PDF buffer to the output file
  fs.writeFileSync(outputPath, new Uint8Array(pdfBytes));
}
