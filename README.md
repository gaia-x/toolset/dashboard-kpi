# Dashboard KPI

UI / API to generate and monitoring KPIs (merge requests, issues) for a list of any gitlab projects in a given period.

(Can be used for any gitlab projects)

#### Using:

- Next.js
- Chakra UI
- Chart JS
- Gitlab API
- Prisma
- Vercel (serverless)

## Dev

Install

```
yarn install
```

Dev

```
yarn dev
```

## Create PNG

```bash
yarn png
```

## Add a new project

`projects.js`

Add a new project in the `projects` array.

```js
{
    name: "My project name",
        projectId
:
    0000000 // Gitlab project ID (available on the project gitlab page)
}
```

## CRON

`vercel.json`

Run each day at 00:00 to update data in database calling `/api/cron`
