import { getKpisAndSave } from "@/getKpisAndSave";
import fs from "fs";

export default async function handler(req, res) {
  const { forceUpdate } = req.body;

  if (forceUpdate) {
    const kpis = await getKpisAndSave();
    return res.send(kpis);
  }

  const kpisFileExists = fs.existsSync("./kpis.json");

  if (kpisFileExists) {
    const file = JSON.parse(fs.readFileSync("./kpis.json", "utf8"));
    const filteredData = file.data.kpis.map((item) => {
      return {
        name: item.name,
        mergeRequests: item.mergeRequests,
        issues: item.issues,
      };
    });

    res.send(filteredData);
  } else {
    const kpis = await getKpisAndSave();
    return res.send(kpis);
  }
}
