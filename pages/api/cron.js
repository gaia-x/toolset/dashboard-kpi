import { getKpisAndSave } from "@/getKpisAndSave";

export default async function handler(req, res) {
  try {
    await getKpisAndSave();
    res.send("success");
  } catch (error) {
    console.error(error);
  }
}
