import { Header } from "@/components/Header";
import { Project } from "@/components/Project";
import { Filter } from "@/components/Filter";
import {
  Box,
  Center,
  CircularProgress,
  Flex,
  useToast,
} from "@chakra-ui/react";
import axios from "axios";
import { useCallback, useEffect, useState } from "react";

export const primaryColor = "#110094";

export default function Home() {
  const [data, setData] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  const toast = useToast();

  const fetchKPIs = useCallback(async (isForceUpdateChecked) => {
    try {
      setIsLoading(true);
      const { data } = await axios.post("/api/kpi", {
        forceUpdate: isForceUpdateChecked,
      });
      setData(data);
    } catch (error) {
      console.log(error);
      toast({
        title: "Error !",
        description: "Error while fetching data...",
        status: "error",
        duration: 9000,
        isClosable: true,
      });
    } finally {
      setIsLoading(false);
    }
  }, []);

  useEffect(() => {
    fetchKPIs();
  }, [fetchKPIs]);

  return (
    <Box px={5}>
      <Header />
      <Filter fetch={fetchKPIs} isLoading={isLoading} />
      <Flex direction={"column"}>
        {isLoading ? (
          <Center mt={5} h={"40vh"}>
            <CircularProgress isIndeterminate color={primaryColor} />
          </Center>
        ) : (
          data.length > 1 &&
          data.map((project) => (
            <Project key={project.name} project={project} />
          ))
        )}
      </Flex>
    </Box>
  );
}
