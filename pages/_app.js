import { ChakraProvider } from "@chakra-ui/react";
import Head from "next/head";

export default function App({ Component, pageProps }) {
  return (
    <ChakraProvider>
      <Head>
        <link rel="shortcut icon" href="/gx-favicon.png" />
      </Head>
      <Component {...pageProps} />
    </ChakraProvider>
  );
}
