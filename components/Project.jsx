import {
  Box,
  Divider,
  Flex,
  Heading,
  HStack,
  Stat,
  StatLabel,
  StatNumber,
  Text,
} from "@chakra-ui/react";
import {
  CategoryScale,
  Chart as ChartJS,
  Legend,
  LinearScale,
  LineElement,
  PointElement,
  Title,
  Tooltip,
} from "chart.js";
import { Chart } from "react-chartjs-2";
import {
  LineWithErrorBarsController,
  PointWithErrorBar,
} from "chartjs-chart-error-bars";
import { useEffect, useState } from "react";

ChartJS.register(
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend,
  LineWithErrorBarsController,
  PointWithErrorBar
);

const months = [
  "Jan",
  "Feb",
  "Mar",
  "Apr",
  "May",
  "Jun",
  "Jul",
  "Aug",
  "Sep",
  "Oct",
  "Nov",
  "Dec",
];

export const Project = ({ project }) => {
  const { mergeRequests, issues } = project;

  const [mrAverages, setMrAverages] = useState();
  const [issuesAverages, setIssuesAverages] = useState();

  const mrValues = Object.values(mergeRequests);
  const issuesValues = Object.values(issues);

  const replaceMonth = (date) => {
    const month = date.split("/")[0];
    return date.replace(month, months[month - 1]).replace("/", " ");
  };

  useEffect(() => {
    const createdAcc = mrValues.reduce((acc, value) => acc + value.created, 0);
    const closedAcc = mrValues.reduce((acc, value) => acc + value.closed, 0);
    const mergedAcc = mrValues.reduce((acc, value) => acc + value.merged, 0);

    setMrAverages({
      createdAverage: createdAcc / mrValues.length,
      closedAverage: closedAcc / mrValues.length,
      mergedAverage: mergedAcc / mrValues.length,
    });
  }, [mrValues]);

  useEffect(() => {
    const createdAcc = issuesValues.reduce(
      (acc, value) => acc + value.created,
      0
    );
    const closedAcc = issuesValues.reduce(
      (acc, value) => acc + value.closed,
      0
    );

    setIssuesAverages({
      createdAverage: createdAcc / issuesValues.length,
      closedAverage: closedAcc / issuesValues.length,
    });
  }, [issuesValues]);

  const MRChartData = {
    labels: Object.keys(mergeRequests).map((date) => replaceMonth(date)),
    datasets: [
      {
        label: "Open",
        data: mrValues.map((value) => ({
          y: value.open,
          yMin: value.open - (value.closed + value.merged),
          yMax: value.open + value.created,
        })),
        backgroundColor: "#110094",
        borderColor: "magenta",
      },
      {
        label: "Min:  Closed + Merged",
      },
      {
        label: "Max: Created",
      },
    ],
  };

  const issuesChartData = {
    labels: Object.keys(issues).map((date) => replaceMonth(date)),
    datasets: [
      {
        label: "Open",
        data: issuesValues.map((value) => ({
          y: value.open,
          yMin: value.open - value.closed,
          yMax: value.open + value.created,
        })),
        backgroundColor: "#110094",
        borderColor: "magenta",
      },
      {
        label: "Min:  Closed",
      },
      {
        label: "Max: Created",
      },
    ],
  };

  return (
    <Box my={3} className={"project"}>
      <Flex w={"100%"} flexDirection={"column"} alignItems={"center"}>
        <Heading size="xl" color="#110094">
          {project.name}
        </Heading>
        <Flex w={"100%"} mt={3}>
          <Flex w={"50%"} justifyContent={"center"}>
            {mrAverages && (
              <HStack spacing={10}>
                <Stat>
                  <StatLabel>Average MR created per month</StatLabel>
                  <StatNumber>
                    {Math.round(mrAverages.createdAverage)}
                  </StatNumber>
                </Stat>
                <Stat>
                  <StatLabel>Average MR closed per month</StatLabel>
                  <StatNumber>
                    {Math.round(mrAverages.closedAverage)}
                  </StatNumber>
                </Stat>
                <Stat>
                  <StatLabel>Average MR merged per month</StatLabel>
                  <StatNumber>
                    {Math.round(mrAverages.mergedAverage)}
                  </StatNumber>
                </Stat>
              </HStack>
            )}
          </Flex>
          <Flex w={"50%"} justifyContent={"center"}>
            {issuesAverages && (
              <HStack spacing={10}>
                <Stat>
                  <StatLabel>Average issues created per month</StatLabel>
                  <StatNumber>
                    {Math.round(issuesAverages.createdAverage)}
                  </StatNumber>
                </Stat>
                <Stat>
                  <StatLabel>Average issues closed per month</StatLabel>
                  <StatNumber>
                    {Math.round(issuesAverages.closedAverage)}
                  </StatNumber>
                </Stat>
              </HStack>
            )}
          </Flex>
        </Flex>
      </Flex>
      <Flex mt={3}>
        <Flex w={"50%"} direction={"column"} alignItems={"center"} px={3}>
          <Text>Merge requests</Text>
          <Chart
            type={LineWithErrorBarsController.id}
            data={MRChartData}
            width={"100%"}
            height={"30%"}
          />
        </Flex>
        <Flex w={"50%"} direction={"column"} alignItems={"center"} px={3}>
          <Text>Issues</Text>
          <Chart
            type={LineWithErrorBarsController.id}
            data={issuesChartData}
            width={"100%"}
            height={"30%"}
          />
        </Flex>
      </Flex>
      <Divider mt={5} orientation={"horizontal"} />
    </Box>
  );
};
