import { Button, Flex } from "@chakra-ui/react";
import { primaryColor } from "@/pages";

export const Filter = ({ fetch, isLoading }) => {
  const handleSearch = async () => {
    await fetch(true);
  };

  return (
    <Flex w={"100%"} p={5} alignItems={"center"}>
      <Flex direction={"column"} m={3}>
        <Button
          isLoading={isLoading}
          onClick={handleSearch}
          colorScheme={"magenta"}
          bgColor={primaryColor}
          my={2}
        >
          Update KPIs
        </Button>
      </Flex>
    </Flex>
  );
};
