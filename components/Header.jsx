import { Box, Flex, Heading, Text } from "@chakra-ui/react";
import Image from "next/image";
import gxLogo from "@/public/gx-logo.png";

export const Header = () => {
  return (
    <Flex direction={"column"}>
      <Flex alignItems={"center"} p={6}>
        <Box boxSize={"80px"}>
          <Image src={gxLogo} alt={"gaia-x-logo"} />
        </Box>
        <Heading color="#110094" ml={3}>
          Gaia-X Gitlab Repositories KPIs
        </Heading>
      </Flex>
      <Text ml={6}>
        This dashboard is designed to provide you with real-time insights into
        our GitLab repositories (MRs & Issues).
      </Text>
    </Flex>
  );
};
